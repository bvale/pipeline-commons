package com.wsgc.devops.pipeline

/**
 * Created by bvale on 08/08/2017.
 */
class Service implements Serializable{

    /**
     * Holds Jenkins pipeline steps class so we can use it inside
     * this package. When testing we can pass a Stub script or
     * use test.Script which provides a valid implementation
     * of the most used Jenkins methods
     */
    def script

    Service(script) {
        this.script = script
    }

    def run(){
        node{
            def mvn = tool name: '3.5.0', type: 'maven'
            def jdk = tool name: '1.8', type: 'jdk'
            def pipelineEnv = ["JAVA_HOME=${jdk}", "PATH+MAVEN=${mvn}/bin:${jdk}/bin"]
            def mvnGoals = 'clean package'
            script.git credentialsId: 'bvale-LDAP', url: 'https://github.wsgc.com/bvale/inventory.git', branch: 'release'
            stage('Build'){
                script.withCredentials([string(credentialsId: 'cacerts', variable: 'foo')]) {
                    script.withEnv(pipelineEnv) {
                        script.sh "mvn --batch-mode -V -U -e ${mvnGoals} " +
                                "-Dsurefire.useFile=false -Djavax.net.ssl.trustStore=/var/jenkins_home/.keystore/cacerts " +
                                "-Djavax.net.ssl.trustStorePassword=${foo}"
                    }
                }
            }
        }
    }
}
